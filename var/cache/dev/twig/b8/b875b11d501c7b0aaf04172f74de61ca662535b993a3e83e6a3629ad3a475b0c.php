<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* main/about.html.twig */
class __TwigTemplate_1f70bc9452a7166d1e3ebe1613a3466a57f5acfadf3d526064be066d47b4ea6c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "main/about.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "main/about.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "main/about.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"\">
\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t<img class=\"rounded-circle\" width=\"300\" height=\"300\" src=\"https://image.shutterstock.com/image-vector/avatar-business-man-personal-commercial-600w-493718947.jpg\">
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">

\t\t\t\t<h3>SAADA Nassim</h3>
\t\t\t\t<p>
\t\t\t\t\tEtudiant en Master2 informatique pour les sciences à l’université de Montpellier,
\t\t\t\t\tpassionné par les nouvelles technologies et le développement web & logiciels, je suis à
\t\t\t\t\tla recherche d’un stage de fin d’étude.
\t\t\t\t</p>
\t\t\t\t\t<h3>FORMATION & DIPLOME</h3>
\t\t\t\t<p>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t2019_2020 : Master 2, Informatique pour les sciences
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t2018 _2019 : Master 1, Informatique pour les sciences
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2017_2018 : DU, traitement de l’information et intelligence économique
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2007_2012 : Licence (Bac+4) en science de gestion, spécialité fiance
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Bejaia
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2006_2007 : Baccalauréat gestion et économie
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Bejaia
\t\t\t\t</p>
\t\t\t\t<h3>Langages</h3>
\t\t\t\t<p>
\t\t\t\t\t
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Anglais 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tBonne connaissance
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Français 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tCourant
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Arabe 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tCourant
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Berbère 
\t\t\t\t\t</Strong>
\t\t\t\t\tLangue maternelle
\t\t\t\t</p>
\t\t\t\t<h3>Hobby</h3>
\t\t\t\t<Strong>
\t\t\t\t\t Lecture
\t\t\t\t\t</br>
\t\t\t\t\t Jeux d’échec
\t\t\t\t\t</br>
\t\t\t\t\t voyage
\t\t\t\t\t</br>
\t\t\t\t\t Mouvement associatif et
\t\t\t\t\torganisationne
\t\t\t\t\t</Strong>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "main/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} {# héritage de template de base #}


{% block body %}
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"\">
\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t<img class=\"rounded-circle\" width=\"300\" height=\"300\" src=\"https://image.shutterstock.com/image-vector/avatar-business-man-personal-commercial-600w-493718947.jpg\">
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">

\t\t\t\t<h3>SAADA Nassim</h3>
\t\t\t\t<p>
\t\t\t\t\tEtudiant en Master2 informatique pour les sciences à l’université de Montpellier,
\t\t\t\t\tpassionné par les nouvelles technologies et le développement web & logiciels, je suis à
\t\t\t\t\tla recherche d’un stage de fin d’étude.
\t\t\t\t</p>
\t\t\t\t\t<h3>FORMATION & DIPLOME</h3>
\t\t\t\t<p>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t2019_2020 : Master 2, Informatique pour les sciences
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t2018 _2019 : Master 1, Informatique pour les sciences
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2017_2018 : DU, traitement de l’information et intelligence économique
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Montpellier
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2007_2012 : Licence (Bac+4) en science de gestion, spécialité fiance
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Bejaia
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t2006_2007 : Baccalauréat gestion et économie
\t\t\t\t\t</Strong>
\t\t\t\t\t</br>
\t\t\t\t\tUniversité de Bejaia
\t\t\t\t</p>
\t\t\t\t<h3>Langages</h3>
\t\t\t\t<p>
\t\t\t\t\t
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Anglais 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tBonne connaissance
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Français 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tCourant
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Arabe 
\t\t\t\t\t</Strong>
\t\t\t\t\t\tCourant
\t\t\t\t\t</br>
\t\t\t\t\t<Strong>
\t\t\t\t\t\t Berbère 
\t\t\t\t\t</Strong>
\t\t\t\t\tLangue maternelle
\t\t\t\t</p>
\t\t\t\t<h3>Hobby</h3>
\t\t\t\t<Strong>
\t\t\t\t\t Lecture
\t\t\t\t\t</br>
\t\t\t\t\t Jeux d’échec
\t\t\t\t\t</br>
\t\t\t\t\t voyage
\t\t\t\t\t</br>
\t\t\t\t\t Mouvement associatif et
\t\t\t\t\torganisationne
\t\t\t\t\t</Strong>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
{% endblock %}

", "main/about.html.twig", "/home/nassimsd/Bureau/M2/TW/SYMPHONY/my_tp_project/templates/main/about.html.twig");
    }
}
