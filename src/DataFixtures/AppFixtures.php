<?php

namespace App\DataFixtures;

use App\Entity\Projets;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{//la fonction load le parametre manager va permettre d'inserer,mettre à jours ou supprimer des lignes dans BD
    public function load(ObjectManager $manager)
    {
        $projet = new Projets();
        $projet->setTitre("site de vente");
        $projet->setDescription("Site de ventes en ligne, tecnolgies utilisées:Angular, Mongo db, javaScript, typeScript,
        SQL, HTML, Boutstrap");
        $projet->setUrl("https://gitlab.com/");
        $projet->setImage("https://creation-et-referencement-de-sites-internet.com/wp-content/uploads/2017/11/Cr%C3%A9ation-de-site-de-vente-en-ligne-ideaxe1.jpg");
        $manager->persist( $projet );

        $projet = new Projets();
        $projet->setTitre("Projet Réseau");
        $projet->setDescription("Simulation de Réseau Informatique d’entreprise");
        $projet->setUrl("https://gitlab.com/");
        $projet->setImage("https://groupenetworks.fr/wp-content/uploads/2017/01/Reseau-informatique.jpg");
        $manager->persist( $projet );

        $projet = new Projets();
        $projet->setTitre("Explorateur de site web");
        $projet->setDescription("Explorateur de site web, tecnolgies utilisées:JavaScript, CGI, HTML, CSS");
        $projet->setUrl("https://gitlab.com/");
        $projet->setImage("https://www.e-monsite.com/medias/images/explorateur.png");
        $manager->persist( $projet );

        $projet = new Projets();
        $projet->setTitre("petites annonces");
        $projet->setDescription("Création d’une application participative petites annonces, tecnolgies utilisées:MySQL, HTML, PHP, CSS, AJAX
        ( Javascript + Webservices");
        $projet->setUrl("https://gitlab.com/");
        $projet->setImage("https://www.fashion-habille-la.com/upload/Les-sites-de-petites-annonces-entre-particuliers.jpg");
        $manager->persist( $projet );
                    //la fonction persist prépare l'envoi des modification vers la BD
        $manager->flush();//la fonction flush crée la requte sql qui va modifier la BD
    }
}
