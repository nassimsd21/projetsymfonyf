<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
   
    /**
     * @Route("/homepage", name="homepage")
     */
    //l annotation ci-dessus va faire appel à la fonction index ci_dessous pour l affichage de la page homepage.html.twig

    public function index()
    {
        return $this->render('main/homepage.html.twig');
    }

     /**
     * @Route("/about", name="about")
     */
    //l annotation ci-dessus va faire appel à la fonction index ci_dessous pour l affichage de la page about.html.twig
    public function about()
    {
        return $this->render('main/about.html.twig');
    }



}
