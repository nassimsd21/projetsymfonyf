<?php

namespace App\Controller;

use App\Entity\Projets;
use App\Repository\ProjetsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjetsController extends AbstractController
{
    
    /**
     * @Route("/projets", name="projets", methods={"GET"})
     */                   //instance de la classe ProjetsRepository 
    public function index(ProjetsRepository $projetsRepository): Response
    {//la fonction qui affiche les projets dans le le template
        return $this->render('projets/index.html.twig', [
            'projets' => $projetsRepository->findAll(),
        ]);
    }

     /**
     * @Route("/projets/{id}", name="projets_show", methods={"GET"})
     */                   
    public function show(Projets $projet): Response /* cette fonction va nous retourner la page qui va afficher le contenu d'un projet */
    {

        return $this->render('projets/show.html.twig', [
            'projet' => $projet,
        ]); //creation d'une variable projets qui va utiliser dans twig pour parcourir les projets et les afficher
    }


    

}
